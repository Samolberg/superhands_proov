package com.example.superhandsproov


import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.component1
import androidx.activity.result.contract.ActivityResultContracts
import com.example.superhandsproov.api.endpoints.RequestApi
import com.example.superhandsproov.api.getRetrofit
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private var api = getRetrofit().create(RequestApi::class.java)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val resultQR = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult? ->
            Log.d("result", result?.component1().toString())
            when {
                result?.component1() == 0 -> {
                    myIP()
                }
                result?.component1() == 1 -> {
                    Tasks(this).openFlashLight(false)
                }
                result?.component1() == 2 -> {
                    Tasks(this).openFlashLight(true)
                }
                result?.component1() == 3 -> {
                    Tasks(this).vibrate(1000)
                }
            }

        }
        ipAddress.setOnClickListener {
            myIP()
        }

        flashlight.setOnClickListener {
            if (flashlight.text == "You are blinding me") {
                Tasks(this).openFlashLight(true)
                flashlight.setText(R.string.get_dark)

            }else{
                Tasks(this).openFlashLight(false)
                flashlight.setText(R.string.blinding)

            }
        }

        vibrate.setOnClickListener {
            Tasks(this).vibrate(1000)


        }
        scanner.setOnClickListener {
            val intent = Intent(this, ScannerActivity::class.java)
            try {
                resultQR.launch(intent)
            }catch (e: java.lang.Exception){


            }
        }
        darkMode.setOnClickListener {
            layout.setBackgroundColor(Color.BLACK)
        }


    }
    private fun myIP(){

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val IPA = api.getAddress().await()
                CoroutineScope(Dispatchers.Main).launch {

                        Toast.makeText(applicationContext, IPA,Toast.LENGTH_LONG).show()

                }
            } catch (e: Exception) {
                Log.e("catch", e.toString())
                CoroutineScope(Dispatchers.Main).launch {

                }
            }

        }
    }
/*    private fun openFlashLight() {
        val cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        val cameraId = cameraManager.cameraIdList[0]
        if (!flashLightStatus) {
            try {
                cameraManager.setTorchMode(cameraId, true)
                flashlight.setText("You are blinding me")
                flashLightStatus = true

            } catch (e: CameraAccessException) {
            }
        } else {
            try {
                cameraManager.setTorchMode(cameraId, false)
                flashLightStatus = false
                flashlight.setText("It's getting dark")

            } catch (e: CameraAccessException) {
            }
        }

    }*/

/*    private fun vibrate(millisecond: Long) {
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(millisecond, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(millisecond)
        }
    }*/



}
