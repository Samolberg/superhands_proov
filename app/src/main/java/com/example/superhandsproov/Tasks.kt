package com.example.superhandsproov

import android.content.Context
import android.content.Context.CAMERA_SERVICE
import android.content.Context.VIBRATOR_SERVICE
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log



class Tasks (val context: Context){


    fun vibrate(millisecond: Long) {
        val vibrator = context.getSystemService(VIBRATOR_SERVICE) as Vibrator

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(millisecond, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            vibrator.vibrate(millisecond)
        }
    }

    fun openFlashLight(flashLightStatus: Boolean) {

        val cameraManager = context.getSystemService(CAMERA_SERVICE) as CameraManager
        val cameraId = cameraManager.cameraIdList[0]
        if (!flashLightStatus) {
            try {
                cameraManager.setTorchMode(cameraId, true)

            } catch (e: CameraAccessException) {

            }
        } else {
            try {
                cameraManager.setTorchMode(cameraId, false)

            } catch (e: CameraAccessException) {

            }
        }

    }
}