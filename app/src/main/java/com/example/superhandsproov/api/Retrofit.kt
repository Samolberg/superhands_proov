package com.example.superhandsproov.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

fun getRetrofit(): Retrofit {

    val httpClientBuilder = OkHttpClient.Builder()
    httpClientBuilder.addInterceptor { chain ->
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.header("Content-Type", "application/json")
        requestBuilder.header("Accept", "application/json")
        chain.proceed(requestBuilder.build())
    }

    OkHttpClient.Builder()
        .addNetworkInterceptor(StethoInterceptor())
        .build()


    val httpClient = httpClientBuilder.build()

    return Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("http://whatismyip.akamai.com")
        .client(httpClient)
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()

}