package com.example.superhandsproov.api.endpoints

import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface RequestApi {

    @GET("/")
    fun getAddress(): Deferred<String>


}
